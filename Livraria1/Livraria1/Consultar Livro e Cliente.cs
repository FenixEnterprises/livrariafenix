﻿using Livraria1.Empréstimo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Livraria1
{
    public partial class LoginCliente : Form
    {
               

        public LoginCliente()
        {
            InitializeComponent();
            
        }

        private void LoginCliente_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                EmprestimoBusiness business = new EmprestimoBusiness();
                List<EmprestimoDTO> Lista = business.Listar(txtEmprestimo.Text);

                dgvLocalizar.AutoGenerateColumns = false;
                dgvLocalizar.DataSource = Lista;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Livraria",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro, tente mais tarde.", "Livraria",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            
        }

        public void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        public void textBox1_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
