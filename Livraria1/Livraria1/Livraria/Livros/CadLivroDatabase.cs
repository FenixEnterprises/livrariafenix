﻿using Livraria1.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria1.DB.Livros
{
    class CadLivroDatabase
    {
        public int Salvar(CadLivroDTO dto)
        {
            string script =
                @"insert into tb_cliente (nm_livro, nm_autor, dt_ano, ds_disponibilidade)
                                  VALUES (@nm_livro, @nm_autor, @dt_ano, @ds_disponibilidade)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_livro", dto.nome));
            parms.Add(new MySqlParameter("nm_autor", dto.autor));
            parms.Add(new MySqlParameter("dt_ano", dto.ano));
            parms.Add(new MySqlParameter("dt_edicao", dto.edicao));
            parms.Add(new MySqlParameter("ds_disponibilidade", dto.disponibilidade));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPK(script, parms);
        }

        public List<CadLivroDTO> Listar()
        {
            string script = "Select*from tb_livros";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CadLivroDTO> lista = new List<CadLivroDTO>();
            while (reader.Read())
            {
                CadLivroDTO dto = new CadLivroDTO();
                dto.ID = reader.GetInt32("id_livro");
                dto.nome = reader.GetString("nm_livro");
                dto.autor = reader.GetString("nm_autor");
                dto.edicao = reader.GetString("ds_edicao");
                dto.ano = reader.GetString("ds_ano");
                dto.disponibilidade = reader.GetBoolean("bt_disponibilidade");

                lista.Add(dto);

            }

            reader.Close();
            return lista;

        }
    }
}
