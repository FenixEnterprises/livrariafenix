﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria1.DB.Livros
{
    class CadLivroDTO
    {
        public int ID { get; set; }
        public string nome { get; set; }
        public string autor { get; set; }
        public string edicao { get; set; }
        public string ano { get; set; }
        public bool disponibilidade { get; set; }


    }
}
