﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria1.DB.Livros
{
    class CadLivroBusiness
    {
        public int Salvar(CadLivroDTO dto)
        {
            if (dto.nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório");
            }

            if (dto.autor == string.Empty)
            {
                throw new ArgumentException("Autor é obrigatório");
            }

            if (dto.edicao == string.Empty)
            {
                throw new ArgumentException("Edição é obrigatório");
            }

            if (dto.ano == string.Empty)
            {
                throw new ArgumentException("Ano é obrigatório");
            }

            CadLivroDatabase db = new CadLivroDatabase();
            return db.Salvar(dto);
        }

        public List<CadLivroDTO> Listar()
        {
            CadLivroDatabase db = new CadLivroDatabase();
            return db.Listar();
        }


    }
}
