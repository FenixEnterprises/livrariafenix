﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria1.Empréstimo
{
    class EmprestimoItemBusiness
    {
        public int Salvar(EmprestimoItemDTO dto)
        {

            if (dto.Emprestimo == DateTime.Now)
            {
                throw new ArgumentException("Emprestimo é obrigatório");
            }
            if (dto.Livro == string.Empty)
            {
                throw new ArgumentException("Livro é obrigatório");
            }
            if (dto.Prazo == DateTime.Now)
            {
                throw new ArgumentException("Prazo é obrigatorio");
            }


            EmprestimoItemDatabase db = new EmprestimoItemDatabase();
            return db.Confirmar(dto);

        }
    }
}
