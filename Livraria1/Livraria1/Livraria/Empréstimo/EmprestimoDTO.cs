﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria1.Empréstimo
{
    class EmprestimoDTO
    {
        public int Id{ get; set; }

        public string Cliente { get; set; }

        public string  Funcionario { get; set; }
                               
        public DateTime Emprestimo { get; set; }

        public DateTime Devolucao { get; set; }


    }
}
