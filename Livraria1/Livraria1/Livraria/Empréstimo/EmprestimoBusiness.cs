﻿using Livraria1.Cliente;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria1.Empréstimo
{
    class EmprestimoBusiness
    {
        public int Salvar(EmprestimoDTO dto)
        {
            if (dto.Cliente == string.Empty)
            {
                throw new ArgumentException("Cliente é obrigatório");
            }
            if (dto.Funcionario == string.Empty)
            {
                throw new ArgumentException("funcionario é obrigatório");
            }
            if (dto.Devolucao ==  DateTime.Now)
            {
                throw new ArgumentException("Devolução é obrigatorio");
            }

            if (dto.Emprestimo == DateTime.Now)
            {
                throw new ArgumentException("Emprestimo é obrigatorio");
            }


            EmprestimoDatabase db = new EmprestimoDatabase();
            return db.Salvar(dto);
        }

        public List<EmprestimoDTO> Listar()
        {
            EmprestimoDatabase db = new EmprestimoDatabase();
            return db.Listar();
        }
    }
}
