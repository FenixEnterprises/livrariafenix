﻿using Livraria1.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria1.Empréstimo
{
    class EmprestimoDatabase
    {
        public int Salvar(EmprestimoDTO dto)
        {
            string script = @"INSERT INTO tb_emprestimo (nm_cliente,nm_funcionario,dt_emprestimo,dt_devolucao) VALUES 
                        (@nm_cliente,@nm_funcionario,@dt_emprestimo,@dt_devolucao)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.Cliente));
            parms.Add(new MySqlParameter("nm_funcionario", dto.Funcionario));
            parms.Add(new MySqlParameter("dt_emprestimo", dto.Emprestimo));
            parms.Add(new MySqlParameter("dt_devolucao", dto.Devolucao));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPK(script, parms);
        }

        public List<EmprestimoDTO> Listar()
        {
            string script = "Select*from tb_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EmprestimoDTO> lista = new List<EmprestimoDTO>();
            while (reader.Read())
            {
                EmprestimoDTO dto = new EmprestimoDTO();
                dto.Id = reader.GetInt32("id_cliente");
                dto.Cliente = reader.GetString("nm_cliente");
                dto.Funcionario = reader.GetString("nm_funcionario");
                dto.Emprestimo = reader.GetDateTime("dt_emprestimo");
                dto.Devolucao = reader.GetDateTime("dt_devolucao");


                lista.Add(dto);



            }

            reader.Close();
            return lista;
        }



    }
}
