﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria1.Empréstimo
{
    class EmprestimoItemDTO
    {
         

        public int Id { get; set; }

        public DateTime Emprestimo { get; set; }

        public string Livro { get; set; }

        public int Quantidade { get; set; }

        public DateTime Prazo { get; set; }

       
    }
}

