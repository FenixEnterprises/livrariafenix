﻿using Livraria1.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria1.Empréstimo
{
    class EmprestimoItemDatabase
    {
        public int Confirmar(EmprestimoItemDTO dto)
        {
            string script = @"INSERT INTO tb_emprestimoitem(dt_emprestimo,nm_livro,ds_quantidade,dt_prazo)VALUES(@dt_emprestimo,@nm_livro,@ds_quantidade,@dt_prazo)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_emprestimo", dto.Emprestimo));
            parms.Add(new MySqlParameter("nm_livro", dto.Livro));
            parms.Add(new MySqlParameter("ds_quantidade", dto.Quantidade));
            parms.Add(new MySqlParameter("dt_prazo", dto.Prazo));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPK(script, parms);
        }


            public List<EmprestimoItemDTO> Listar()
        {
            string script = "Select*from tb_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EmprestimoItemDTO> lista = new List<EmprestimoItemDTO>();
            while (reader.Read())
            {
                EmprestimoItemDTO dto = new EmprestimoItemDTO();
                dto.Id = reader.GetInt32("id_emprestimo");
                dto.Emprestimo = reader.GetDateTime("dt_emprestimo");
                dto.Livro = reader.GetString("nm_livro");
                dto.Prazo = reader.GetDateTime("dt_prazo");



                lista.Add(dto);



            }

            reader.Close();
            return lista;

        }
    }
}
