﻿using Livraria1.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria1.DB.Multa
{
    class MultaDatabase
    {
        public int Salvar (MultaDTO dto)
            {
            string script =
                @"insert into tb_multa(id_multa, id_emprestimo, vl_valor, ds_descricao)
VALUES(@id_multa, @id_emprestimo, @vl_valor, @ds_descricao)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_multa", dto.multa));
            parms.Add(new MySqlParameter("id_emprestimo", dto.emprestimo));
            parms.Add(new MySqlParameter("vl_valor", dto.valor));
            parms.Add(new MySqlParameter("ds_descricao", dto.descricao));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPK(script, parms);

          
        }
            
    }
}
