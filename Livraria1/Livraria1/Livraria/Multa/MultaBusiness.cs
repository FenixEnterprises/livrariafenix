﻿using Livraria1.DB.Multa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria1.DB.Multa
{
    class MultaBusiness
    {
        public int salvar(MultaDTO dto)
   
        {

            
            if (dto.multa == string.Empty)
            {
                throw new ArgumentException("Multa é obrigatório");
            }

            if (dto.emprestimo == string.Empty)
            {
                throw new ArgumentException("Empréstimo é obrigatório");
            }

            if (dto.valor == 0)
            {
                throw new ArgumentException("Valor é obrigatório");
            }


            if (dto.descricao == string.Empty)
            {
                throw new ArgumentException("Descrição é obrigatório");
            }
 

             MultaDatabase db = new MultaDatabase ();
             return db.Salvar(dto); 
}
    }
        }
