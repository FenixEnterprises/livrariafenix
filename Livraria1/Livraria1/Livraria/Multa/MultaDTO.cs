﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria1.DB.Multa
{
    class MultaDTO
    {
        public int id { get; set; }
        public string multa { get; set; }
        public string emprestimo { get; set; }
        public int valor { get; set; }
        public string descricao { get; set; }
    }
}
