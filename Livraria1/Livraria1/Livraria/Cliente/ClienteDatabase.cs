﻿using Livraria1.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria1.Cliente
{
    class ClienteDatabase
    {
        public int Salvar(ClienteDTO dto)
        {
            string script =
                @"insert into tb_cliente (nm_cliente, ds_idade, ds_endereco, ds_sexo, ds_CPF,ds_telefone)
                VALUES (@nm_pessoa, @ds_idade, @ds_endereco, @ds_sexo, @ds_CPF,@ds_telefone)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.nome));
            parms.Add(new MySqlParameter("ds_idade", dto.idade));
            parms.Add(new MySqlParameter("ds_endereco", dto.endereco));
            parms.Add(new MySqlParameter("ds_sexo", dto.sexo));
            parms.Add(new MySqlParameter("ds_CPF", dto.CPF));
            parms.Add(new MySqlParameter("ds_cidade", dto.cidade));
            parms.Add(new MySqlParameter("ds_rua", dto.rua));
            parms.Add(new MySqlParameter("ds_telefone",dto.telefone));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPK(script, parms);
        }

        public List<ClienteDTO> Listar()
        {
            string script = @"SELECT * FROM tb_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.ID = reader.GetInt32("id_cliente");
                dto.nome = reader.GetString("nm_cliente");
                dto.CPF = reader.GetString("ds_cpf");
                dto.idade =reader.GetString("ds_idade");
                dto.endereco = reader.GetString("ds_endereço");
                dto.cidade = reader.GetString("ds_cidade");
                dto.telefone = reader.GetString("ds_telefone");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


    }
}
