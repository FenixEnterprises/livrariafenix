﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria1.Cliente
{
    class ClienteDTO
    {
        public int ID { get; set; }
        public string nome { get; set; }
        public string endereco { get; set; }
        public string idade { get; set; }
        public string sexo { get; set; }
        public string CPF { get; set; }
        public string cidade { get; set; }
        public string rua { get; set; }
        public string telefone { get; set; }
    }
}
