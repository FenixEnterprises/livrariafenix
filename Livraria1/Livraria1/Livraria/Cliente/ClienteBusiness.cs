﻿using Livraria1.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria1.Cliente
{
    class ClienteBusiness
    {
        public int Salvar(ClienteDTO dto)
        {
            if (dto != null)
            {
                throw new ArgumentException("Cliente já cadastrado no sistema.");
            }
            

            ClienteDatabase db = new ClienteDatabase();
            return db.Salvar(dto);
        }

      
        public List<ClienteDTO> Listar()
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.Listar();
        }


        

    }
}
