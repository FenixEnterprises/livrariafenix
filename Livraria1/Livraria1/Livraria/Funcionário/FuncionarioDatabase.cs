﻿using Livraria1.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria1.DB.Funcionário
{
    class FuncionarioDatabase
    {
        public int Salvar(FuncionarioDTO dto)
        {
            string script =
                @"insert into tb_cliente (nm_funcionario, ds_endereco, ds_telefone, ds_cpf)
                                  VALUES (@nm_funcionario, @ds_endereco, @ds_telefone, @ds_cpf)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", dto.nome));
            parms.Add(new MySqlParameter("ds_endereco", dto.endereco));
            parms.Add(new MySqlParameter("ds_telefone", dto.telefone));
            parms.Add(new MySqlParameter("ds_cpf", dto.cpf));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPK(script, parms);
        }

        public List<FuncionarioDTO> Listar()
        {
            string script = "Select*from tb_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.ID = reader.GetInt32("id_funcionario");
                dto.nome = reader.GetString("nm_funcionario");
                dto.endereco = reader.GetString("ds_endereco");
                dto.telefone = reader.GetString("ds_telefone");
                dto.cpf = reader.GetString("ds_cpf");
                

                lista.Add(dto);
                
                

            }

            reader.Close();
            return lista;

        }

    }
}
