﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria1.DB.Funcionário
{
    class FuncionarioBusiness
    {
        public int Salvar(FuncionarioDTO dto)
        {
            if (dto.nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório");
            }

            if (dto.endereco == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório");
            }
            if (dto.cpf == string.Empty)
            {
                throw new ArgumentException("Cpf é obrigatório");
            }
            if (dto.telefone == string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório");
            }


            FuncionarioDatabase db = new FuncionarioDatabase();

            return db.Salvar(dto);


        }

    }
}
