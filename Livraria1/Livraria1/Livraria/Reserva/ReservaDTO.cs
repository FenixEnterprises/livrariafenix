﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria1.DB.Reserva
{
    class ReservaDTO
    {
        public int id  { get; set; }
        public int reserva { get; set; }
        public string livro { get; set; }
        public string funcionario { get; set; }
        public string cliente { get; set; }
    }
}
