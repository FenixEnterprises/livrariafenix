﻿using Livraria1.DB.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria1.DB.Reserva
{
    class ReservaDatabase
    {
        public int Salvar(ReservaDTO dto)
        {
            string script =
                @"insert into tb_reserva(nm_livro, nm_funcionario)
                VALUES(@nm_livro, @nm_funcionario)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_livro", dto.cliente));
            parms.Add(new MySqlParameter("nm_funcionario", dto.funcionario));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPK(script, parms);
        }


    }
}
