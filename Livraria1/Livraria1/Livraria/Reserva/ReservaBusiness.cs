﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Livraria1.DB.Reserva
{
    class ReservaBusiness
    {
        public int Salvar(ReservaDTO dto)
        {

            if (dto.livro == string.Empty)
            {
                throw new ArgumentException("livro é obrigatório");
            }
            if (dto.funcionario == string.Empty)
            {
                throw new ArgumentException("funcionario é obrigatório");
            }

            ReservaDatabase db = new ReservaDatabase();
            return db.Salvar(dto);
        }
    }
}
