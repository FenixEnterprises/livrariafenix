﻿using Livraria1.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Livraria1
{
    public partial class Login_Cliente : Form
    {
        public Login_Cliente()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtCliente_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            {
                try
                {
                    ClienteBusiness business = new ClienteBusiness();
                    ClienteDTO cliente = business.Listar(txtCliente.Text, txtSenha.Text);

                    if (cliente != null)
                    {
                        UserSession.UsuarioLogado = cliente;

                        frmMenu menu = new frmMenu();
                        menu.Show();
                        this.Hide();

                    }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(ex.Message, "Livraria",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ocorreru um erro, tente mais tarde.", "Livraria",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
