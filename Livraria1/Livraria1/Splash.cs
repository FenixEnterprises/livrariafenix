﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Livraria1
{
    public partial class Splash : Form
    {
        public Splash()
        {
            InitializeComponent();

            // Inicia contagem para término da Splash Screen
            Task.Factory.StartNew(() =>
            {
                // Espera 2 segundos para iniciar o sistema
                System.Threading.Thread.Sleep(5000);

                Invoke(new Action(() =>
                {
                    // Abre a tela Inicial
                    frmMenu frm = new frmMenu();
                    frm.Show();
                    Hide();
                }));
            });
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Splash_Load(object sender, EventArgs e)
        {

        }
    }
}
