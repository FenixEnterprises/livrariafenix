﻿namespace Livraria1
{
    partial class Reserva
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnreserva = new System.Windows.Forms.Button();
            this.txtnomefuncionario = new System.Windows.Forms.TextBox();
            this.txtnomelivro = new System.Windows.Forms.TextBox();
            this.lblnomelivro = new System.Windows.Forms.Label();
            this.lblfuncionario = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnreserva
            // 
            this.btnreserva.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnreserva.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnreserva.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnreserva.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnreserva.Location = new System.Drawing.Point(132, 234);
            this.btnreserva.Name = "btnreserva";
            this.btnreserva.Size = new System.Drawing.Size(159, 57);
            this.btnreserva.TabIndex = 36;
            this.btnreserva.Text = "Reservar";
            this.btnreserva.UseVisualStyleBackColor = false;
            this.btnreserva.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtnomefuncionario
            // 
            this.txtnomefuncionario.Location = new System.Drawing.Point(115, 150);
            this.txtnomefuncionario.Name = "txtnomefuncionario";
            this.txtnomefuncionario.Size = new System.Drawing.Size(248, 20);
            this.txtnomefuncionario.TabIndex = 32;
            this.txtnomefuncionario.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // txtnomelivro
            // 
            this.txtnomelivro.Location = new System.Drawing.Point(115, 103);
            this.txtnomelivro.Name = "txtnomelivro";
            this.txtnomelivro.Size = new System.Drawing.Size(248, 20);
            this.txtnomelivro.TabIndex = 31;
            this.txtnomelivro.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // lblnomelivro
            // 
            this.lblnomelivro.AutoSize = true;
            this.lblnomelivro.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblnomelivro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblnomelivro.Font = new System.Drawing.Font("Poor Richard", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnomelivro.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblnomelivro.Location = new System.Drawing.Point(39, 102);
            this.lblnomelivro.Name = "lblnomelivro";
            this.lblnomelivro.Size = new System.Drawing.Size(45, 19);
            this.lblnomelivro.TabIndex = 28;
            this.lblnomelivro.Text = "Livro";
            // 
            // lblfuncionario
            // 
            this.lblfuncionario.AutoSize = true;
            this.lblfuncionario.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lblfuncionario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblfuncionario.Font = new System.Drawing.Font("Poor Richard", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfuncionario.ForeColor = System.Drawing.SystemColors.Control;
            this.lblfuncionario.Location = new System.Drawing.Point(18, 149);
            this.lblfuncionario.Name = "lblfuncionario";
            this.lblfuncionario.Size = new System.Drawing.Size(91, 19);
            this.lblfuncionario.TabIndex = 26;
            this.lblfuncionario.Text = "Funcionario";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Livraria1.Properties.Resources.tumblr_lrqaeeKXhZ1qbcporo1_500;
            this.pictureBox1.Location = new System.Drawing.Point(-2, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(489, 409);
            this.pictureBox1.TabIndex = 37;
            this.pictureBox1.TabStop = false;
            // 
            // Reserva
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(371, 351);
            this.Controls.Add(this.btnreserva);
            this.Controls.Add(this.txtnomefuncionario);
            this.Controls.Add(this.txtnomelivro);
            this.Controls.Add(this.lblnomelivro);
            this.Controls.Add(this.lblfuncionario);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Reserva";
            this.Text = "Reserva";
            this.Load += new System.EventHandler(this.Reserva_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnreserva;
        private System.Windows.Forms.TextBox txtnomefuncionario;
        private System.Windows.Forms.TextBox txtnomelivro;
        private System.Windows.Forms.Label lblnomelivro;
        private System.Windows.Forms.Label lblfuncionario;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}