﻿namespace Livraria1
{
    partial class Emprestimo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnsalvar = new System.Windows.Forms.Button();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.txtCliente = new System.Windows.Forms.TextBox();
            this.lblFuncionario = new System.Windows.Forms.Label();
            this.lblCliente = new System.Windows.Forms.Label();
            this.lblDataDeDevolucao = new System.Windows.Forms.Label();
            this.lblDataDeEmprestimo = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnsalvar
            // 
            this.btnsalvar.BackColor = System.Drawing.Color.White;
            this.btnsalvar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsalvar.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsalvar.ForeColor = System.Drawing.Color.Goldenrod;
            this.btnsalvar.Location = new System.Drawing.Point(145, 307);
            this.btnsalvar.Name = "btnsalvar";
            this.btnsalvar.Size = new System.Drawing.Size(131, 61);
            this.btnsalvar.TabIndex = 36;
            this.btnsalvar.Text = "Salvar";
            this.btnsalvar.UseVisualStyleBackColor = false;
            this.btnsalvar.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(152, 109);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(200, 20);
            this.textBox3.TabIndex = 33;
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // txtCliente
            // 
            this.txtCliente.Location = new System.Drawing.Point(104, 39);
            this.txtCliente.Name = "txtCliente";
            this.txtCliente.Size = new System.Drawing.Size(200, 20);
            this.txtCliente.TabIndex = 31;
            // 
            // lblFuncionario
            // 
            this.lblFuncionario.AutoSize = true;
            this.lblFuncionario.BackColor = System.Drawing.Color.Gold;
            this.lblFuncionario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblFuncionario.Font = new System.Drawing.Font("Poor Richard", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuncionario.Location = new System.Drawing.Point(7, 108);
            this.lblFuncionario.Name = "lblFuncionario";
            this.lblFuncionario.Size = new System.Drawing.Size(91, 19);
            this.lblFuncionario.TabIndex = 30;
            this.lblFuncionario.Text = "Funcionario";
            // 
            // lblCliente
            // 
            this.lblCliente.AutoSize = true;
            this.lblCliente.BackColor = System.Drawing.Color.Gold;
            this.lblCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblCliente.Font = new System.Drawing.Font("Poor Richard", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(35, 41);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(58, 18);
            this.lblCliente.TabIndex = 28;
            this.lblCliente.Text = "Cliente";
            // 
            // lblDataDeDevolucao
            // 
            this.lblDataDeDevolucao.AutoSize = true;
            this.lblDataDeDevolucao.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblDataDeDevolucao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblDataDeDevolucao.Font = new System.Drawing.Font("Poor Richard", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataDeDevolucao.Location = new System.Drawing.Point(8, 220);
            this.lblDataDeDevolucao.Name = "lblDataDeDevolucao";
            this.lblDataDeDevolucao.Size = new System.Drawing.Size(138, 19);
            this.lblDataDeDevolucao.TabIndex = 27;
            this.lblDataDeDevolucao.Text = "Data da devolução";
            this.lblDataDeDevolucao.Click += new System.EventHandler(this.label3_Click);
            // 
            // lblDataDeEmprestimo
            // 
            this.lblDataDeEmprestimo.AutoSize = true;
            this.lblDataDeEmprestimo.BackColor = System.Drawing.Color.Gold;
            this.lblDataDeEmprestimo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblDataDeEmprestimo.Font = new System.Drawing.Font("Poor Richard", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataDeEmprestimo.Location = new System.Drawing.Point(8, 168);
            this.lblDataDeEmprestimo.Name = "lblDataDeEmprestimo";
            this.lblDataDeEmprestimo.Size = new System.Drawing.Size(138, 18);
            this.lblDataDeEmprestimo.TabIndex = 26;
            this.lblDataDeEmprestimo.Text = "Data de emprestimo";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(152, 220);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker1.TabIndex = 37;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(152, 165);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker2.TabIndex = 38;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Livraria1.Properties.Resources._1f982c4f56bdad3c2736877a62accaf8;
            this.pictureBox1.Location = new System.Drawing.Point(-2, -2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(412, 404);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 39;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // Emprestimo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(408, 400);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.btnsalvar);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.txtCliente);
            this.Controls.Add(this.lblFuncionario);
            this.Controls.Add(this.lblCliente);
            this.Controls.Add(this.lblDataDeDevolucao);
            this.Controls.Add(this.lblDataDeEmprestimo);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Emprestimo";
            this.Text = "Emprestimo";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnsalvar;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox txtCliente;
        private System.Windows.Forms.Label lblFuncionario;
        private System.Windows.Forms.Label lblCliente;
        private System.Windows.Forms.Label lblDataDeDevolucao;
        private System.Windows.Forms.Label lblDataDeEmprestimo;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}