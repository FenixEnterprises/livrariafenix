﻿using Livraria1.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Livraria1
{
    public partial class Cadastrar_Cliente : Form
    {

        public Cadastrar_Cliente()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ClienteDTO dto = new ClienteDTO();
            dto.nome = txtnome.Text;
            dto.idade = txtsexoo.Text;
            dto.sexo = txtsexoo.Text;
            dto.endereco = txtendereco.Text;
            dto.CPF = mskTextBoxcpf.Text;

            ClienteBusiness business = new ClienteBusiness();
            business.Salvar(dto);

            MessageBox.Show("O cadastro foi efetuado com sucesso");
        }

        private void Cadastra_Cliente_Load(object sender, EventArgs e)
        {
            Close();
        }

        private void txtnome_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtidade_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtendereco_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtsexo_TextChanged(object sender, EventArgs e)
        {

        }

        private void mskTextBoxcpf_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void lblEndereco_Click(object sender, EventArgs e)
        {

        }
    }
}
