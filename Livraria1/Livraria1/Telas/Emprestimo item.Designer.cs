﻿namespace Livraria1
{
    partial class Emprestimo_item
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.txtquantidade = new System.Windows.Forms.TextBox();
            this.txtlivro = new System.Windows.Forms.TextBox();
            this.txtemprestimo = new System.Windows.Forms.TextBox();
            this.lblPrazo = new System.Windows.Forms.Label();
            this.lblEmprestimo = new System.Windows.Forms.Label();
            this.lblQuantidade = new System.Windows.Forms.Label();
            this.lblLivro = new System.Windows.Forms.Label();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.BackColor = System.Drawing.Color.Sienna;
            this.btnConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmar.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmar.ForeColor = System.Drawing.Color.Goldenrod;
            this.btnConfirmar.Location = new System.Drawing.Point(134, 264);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(159, 57);
            this.btnConfirmar.TabIndex = 36;
            this.btnConfirmar.Text = "Confirmar";
            this.btnConfirmar.UseVisualStyleBackColor = false;
            this.btnConfirmar.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtquantidade
            // 
            this.txtquantidade.Location = new System.Drawing.Point(134, 150);
            this.txtquantidade.Name = "txtquantidade";
            this.txtquantidade.Size = new System.Drawing.Size(236, 20);
            this.txtquantidade.TabIndex = 35;
            // 
            // txtlivro
            // 
            this.txtlivro.Location = new System.Drawing.Point(134, 100);
            this.txtlivro.Name = "txtlivro";
            this.txtlivro.Size = new System.Drawing.Size(236, 20);
            this.txtlivro.TabIndex = 32;
            // 
            // txtemprestimo
            // 
            this.txtemprestimo.Location = new System.Drawing.Point(134, 57);
            this.txtemprestimo.Name = "txtemprestimo";
            this.txtemprestimo.Size = new System.Drawing.Size(236, 20);
            this.txtemprestimo.TabIndex = 31;
            this.txtemprestimo.TextChanged += new System.EventHandler(this.txtemprestimo_TextChanged);
            // 
            // lblPrazo
            // 
            this.lblPrazo.AutoSize = true;
            this.lblPrazo.BackColor = System.Drawing.Color.Sienna;
            this.lblPrazo.Font = new System.Drawing.Font("Poor Richard", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrazo.ForeColor = System.Drawing.Color.Goldenrod;
            this.lblPrazo.Location = new System.Drawing.Point(34, 199);
            this.lblPrazo.Name = "lblPrazo";
            this.lblPrazo.Size = new System.Drawing.Size(46, 19);
            this.lblPrazo.TabIndex = 30;
            this.lblPrazo.Text = "Prazo";
            // 
            // lblEmprestimo
            // 
            this.lblEmprestimo.AutoSize = true;
            this.lblEmprestimo.BackColor = System.Drawing.Color.Sienna;
            this.lblEmprestimo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblEmprestimo.Font = new System.Drawing.Font("Poor Richard", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmprestimo.ForeColor = System.Drawing.Color.Goldenrod;
            this.lblEmprestimo.Location = new System.Drawing.Point(35, 57);
            this.lblEmprestimo.Name = "lblEmprestimo";
            this.lblEmprestimo.Size = new System.Drawing.Size(85, 18);
            this.lblEmprestimo.TabIndex = 28;
            this.lblEmprestimo.Text = "Emprestimo";
            // 
            // lblQuantidade
            // 
            this.lblQuantidade.AutoSize = true;
            this.lblQuantidade.BackColor = System.Drawing.Color.Sienna;
            this.lblQuantidade.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblQuantidade.Font = new System.Drawing.Font("Poor Richard", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantidade.ForeColor = System.Drawing.Color.Goldenrod;
            this.lblQuantidade.Location = new System.Drawing.Point(34, 152);
            this.lblQuantidade.Name = "lblQuantidade";
            this.lblQuantidade.Size = new System.Drawing.Size(86, 18);
            this.lblQuantidade.TabIndex = 27;
            this.lblQuantidade.Text = "Quantidade";
            // 
            // lblLivro
            // 
            this.lblLivro.AutoSize = true;
            this.lblLivro.BackColor = System.Drawing.Color.Sienna;
            this.lblLivro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblLivro.Font = new System.Drawing.Font("Poor Richard", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLivro.ForeColor = System.Drawing.Color.Goldenrod;
            this.lblLivro.Location = new System.Drawing.Point(35, 107);
            this.lblLivro.Name = "lblLivro";
            this.lblLivro.Size = new System.Drawing.Size(44, 18);
            this.lblLivro.TabIndex = 26;
            this.lblLivro.Text = "Livro";
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Location = new System.Drawing.Point(134, 199);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(236, 20);
            this.dateTimePicker3.TabIndex = 37;
            this.dateTimePicker3.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Livraria1.Properties.Resources.Biblioteca;
            this.pictureBox1.Location = new System.Drawing.Point(1, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(886, 433);
            this.pictureBox1.TabIndex = 38;
            this.pictureBox1.TabStop = false;
            // 
            // Emprestimo_item
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(413, 380);
            this.Controls.Add(this.dateTimePicker3);
            this.Controls.Add(this.btnConfirmar);
            this.Controls.Add(this.txtquantidade);
            this.Controls.Add(this.txtlivro);
            this.Controls.Add(this.txtemprestimo);
            this.Controls.Add(this.lblPrazo);
            this.Controls.Add(this.lblEmprestimo);
            this.Controls.Add(this.lblQuantidade);
            this.Controls.Add(this.lblLivro);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Emprestimo_item";
            this.Text = "Emprestimo_item";
            this.Load += new System.EventHandler(this.Emprestimo_item_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.TextBox txtquantidade;
        private System.Windows.Forms.TextBox txtlivro;
        private System.Windows.Forms.TextBox txtemprestimo;
        private System.Windows.Forms.Label lblPrazo;
        private System.Windows.Forms.Label lblEmprestimo;
        private System.Windows.Forms.Label lblQuantidade;
        private System.Windows.Forms.Label lblLivro;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}