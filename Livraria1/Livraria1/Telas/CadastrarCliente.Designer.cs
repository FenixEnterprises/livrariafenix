﻿namespace Livraria1
{
    partial class Cadastrar_Cliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIdade = new System.Windows.Forms.Label();
            this.lblCPF = new System.Windows.Forms.Label();
            this.lblEndereco = new System.Windows.Forms.Label();
            this.lblNome = new System.Windows.Forms.Label();
            this.lblSexo = new System.Windows.Forms.Label();
            this.btnCadastrar1 = new System.Windows.Forms.Button();
            this.mskTextBoxcpf = new System.Windows.Forms.MaskedTextBox();
            this.txtendereco = new System.Windows.Forms.TextBox();
            this.txtsexoo = new System.Windows.Forms.TextBox();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lblCidade = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.lblRua = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblIdade
            // 
            this.lblIdade.AutoSize = true;
            this.lblIdade.Font = new System.Drawing.Font("Poor Richard", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIdade.Location = new System.Drawing.Point(12, 91);
            this.lblIdade.Name = "lblIdade";
            this.lblIdade.Size = new System.Drawing.Size(45, 18);
            this.lblIdade.TabIndex = 0;
            this.lblIdade.Text = "Idade";
            // 
            // lblCPF
            // 
            this.lblCPF.AutoSize = true;
            this.lblCPF.Font = new System.Drawing.Font("Poor Richard", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPF.Location = new System.Drawing.Point(142, 93);
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.Size = new System.Drawing.Size(44, 18);
            this.lblCPF.TabIndex = 1;
            this.lblCPF.Text = "C.P.F.";
            // 
            // lblEndereco
            // 
            this.lblEndereco.AutoSize = true;
            this.lblEndereco.Font = new System.Drawing.Font("Poor Richard", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndereco.Location = new System.Drawing.Point(297, 11);
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.Size = new System.Drawing.Size(70, 18);
            this.lblEndereco.TabIndex = 2;
            this.lblEndereco.Text = "Endereço";
            this.lblEndereco.Click += new System.EventHandler(this.lblEndereco_Click);
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Font = new System.Drawing.Font("Poor Richard", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.Location = new System.Drawing.Point(12, 9);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(42, 16);
            this.lblNome.TabIndex = 3;
            this.lblNome.Text = "Nome";
            // 
            // lblSexo
            // 
            this.lblSexo.AutoSize = true;
            this.lblSexo.Font = new System.Drawing.Font("Poor Richard", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSexo.Location = new System.Drawing.Point(12, 47);
            this.lblSexo.Name = "lblSexo";
            this.lblSexo.Size = new System.Drawing.Size(39, 18);
            this.lblSexo.TabIndex = 5;
            this.lblSexo.Text = "Sexo";
            // 
            // btnCadastrar1
            // 
            this.btnCadastrar1.BackColor = System.Drawing.Color.SlateBlue;
            this.btnCadastrar1.FlatAppearance.BorderSize = 0;
            this.btnCadastrar1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrar1.Font = new System.Drawing.Font("Poor Richard", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar1.ForeColor = System.Drawing.Color.Gold;
            this.btnCadastrar1.Location = new System.Drawing.Point(276, 315);
            this.btnCadastrar1.Name = "btnCadastrar1";
            this.btnCadastrar1.Size = new System.Drawing.Size(159, 57);
            this.btnCadastrar1.TabIndex = 12;
            this.btnCadastrar1.Text = "Cadastrar";
            this.btnCadastrar1.UseVisualStyleBackColor = false;
            this.btnCadastrar1.Click += new System.EventHandler(this.button1_Click);
            // 
            // mskTextBoxcpf
            // 
            this.mskTextBoxcpf.Location = new System.Drawing.Point(203, 93);
            this.mskTextBoxcpf.Mask = "000-000-000-00";
            this.mskTextBoxcpf.Name = "mskTextBoxcpf";
            this.mskTextBoxcpf.Size = new System.Drawing.Size(91, 20);
            this.mskTextBoxcpf.TabIndex = 13;
            this.mskTextBoxcpf.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.mskTextBoxcpf_MaskInputRejected);
            // 
            // txtendereco
            // 
            this.txtendereco.Location = new System.Drawing.Point(373, 11);
            this.txtendereco.Name = "txtendereco";
            this.txtendereco.Size = new System.Drawing.Size(117, 20);
            this.txtendereco.TabIndex = 15;
            this.txtendereco.TextChanged += new System.EventHandler(this.txtendereco_TextChanged);
            // 
            // txtsexoo
            // 
            this.txtsexoo.Location = new System.Drawing.Point(71, 45);
            this.txtsexoo.Name = "txtsexoo";
            this.txtsexoo.Size = new System.Drawing.Size(97, 20);
            this.txtsexoo.TabIndex = 16;
            this.txtsexoo.TextChanged += new System.EventHandler(this.txtidade_TextChanged);
            // 
            // txtnome
            // 
            this.txtnome.Location = new System.Drawing.Point(71, 9);
            this.txtnome.Multiline = true;
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(214, 20);
            this.txtnome.TabIndex = 17;
            this.txtnome.TextChanged += new System.EventHandler(this.txtnome_TextChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Livraria1.Properties.Resources.lisathegreek6;
            this.pictureBox1.Location = new System.Drawing.Point(2, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(512, 383);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(71, 91);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(45, 20);
            this.textBox1.TabIndex = 19;
            // 
            // lblCidade
            // 
            this.lblCidade.AutoSize = true;
            this.lblCidade.Font = new System.Drawing.Font("Poor Richard", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidade.Location = new System.Drawing.Point(216, 49);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.Size = new System.Drawing.Size(56, 18);
            this.lblCidade.TabIndex = 20;
            this.lblCidade.Text = "Cidade";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(290, 49);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 21;
            // 
            // lblRua
            // 
            this.lblRua.AutoSize = true;
            this.lblRua.Font = new System.Drawing.Font("Poor Richard", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRua.Location = new System.Drawing.Point(325, 91);
            this.lblRua.Name = "lblRua";
            this.lblRua.Size = new System.Drawing.Size(42, 22);
            this.lblRua.TabIndex = 22;
            this.lblRua.Text = "Rua";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(373, 94);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 23;
            // 
            // Cadastrar_Cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 381);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.lblRua);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.lblCidade);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnCadastrar1);
            this.Controls.Add(this.mskTextBoxcpf);
            this.Controls.Add(this.lblCPF);
            this.Controls.Add(this.lblSexo);
            this.Controls.Add(this.txtendereco);
            this.Controls.Add(this.lblEndereco);
            this.Controls.Add(this.lblIdade);
            this.Controls.Add(this.txtsexoo);
            this.Controls.Add(this.lblNome);
            this.Controls.Add(this.txtnome);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Cadastrar_Cliente";
            this.Text = "Cadastra_Cliente";
            this.Load += new System.EventHandler(this.Cadastra_Cliente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblIdade;
        private System.Windows.Forms.Label lblCPF;
        private System.Windows.Forms.Label lblEndereco;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Label lblSexo;
        private System.Windows.Forms.Button btnCadastrar1;
        private System.Windows.Forms.MaskedTextBox mskTextBoxcpf;
        private System.Windows.Forms.TextBox txtendereco;
        private System.Windows.Forms.TextBox txtsexoo;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lblCidade;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label lblRua;
        private System.Windows.Forms.TextBox textBox3;
    }
}