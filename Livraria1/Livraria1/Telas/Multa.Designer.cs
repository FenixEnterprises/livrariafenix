﻿namespace Livraria1
{
    partial class Multa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancelar = new System.Windows.Forms.Button();
            this.txtvalor = new System.Windows.Forms.TextBox();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.lblcliente = new System.Windows.Forms.Label();
            this.lblvalor = new System.Windows.Forms.Label();
            this.lbldescricao = new System.Windows.Forms.Label();
            this.btnconfirmar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtdescricaop = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.Location = new System.Drawing.Point(84, 231);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(159, 57);
            this.btnCancelar.TabIndex = 36;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtvalor
            // 
            this.txtvalor.Location = new System.Drawing.Point(378, 20);
            this.txtvalor.Name = "txtvalor";
            this.txtvalor.Size = new System.Drawing.Size(165, 20);
            this.txtvalor.TabIndex = 32;
            this.txtvalor.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // txtnome
            // 
            this.txtnome.Location = new System.Drawing.Point(66, 20);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(207, 20);
            this.txtnome.TabIndex = 31;
            this.txtnome.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // lblcliente
            // 
            this.lblcliente.AutoSize = true;
            this.lblcliente.BackColor = System.Drawing.Color.White;
            this.lblcliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblcliente.Font = new System.Drawing.Font("Poor Richard", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcliente.Location = new System.Drawing.Point(2, 20);
            this.lblcliente.Name = "lblcliente";
            this.lblcliente.Size = new System.Drawing.Size(58, 18);
            this.lblcliente.TabIndex = 28;
            this.lblcliente.Text = "Cliente";
            // 
            // lblvalor
            // 
            this.lblvalor.AutoSize = true;
            this.lblvalor.BackColor = System.Drawing.Color.White;
            this.lblvalor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblvalor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblvalor.Location = new System.Drawing.Point(325, 20);
            this.lblvalor.Name = "lblvalor";
            this.lblvalor.Size = new System.Drawing.Size(47, 18);
            this.lblvalor.TabIndex = 26;
            this.lblvalor.Text = "Valor";
            this.lblvalor.Click += new System.EventHandler(this.lblvalor_Click);
            // 
            // lbldescricao
            // 
            this.lbldescricao.AutoSize = true;
            this.lbldescricao.BackColor = System.Drawing.Color.White;
            this.lbldescricao.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lbldescricao.Font = new System.Drawing.Font("Poor Richard", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldescricao.Location = new System.Drawing.Point(2, 80);
            this.lbldescricao.Name = "lbldescricao";
            this.lbldescricao.Size = new System.Drawing.Size(74, 18);
            this.lbldescricao.TabIndex = 38;
            this.lbldescricao.Text = "Descrição";
            // 
            // btnconfirmar
            // 
            this.btnconfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnconfirmar.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnconfirmar.Location = new System.Drawing.Point(344, 231);
            this.btnconfirmar.Name = "btnconfirmar";
            this.btnconfirmar.Size = new System.Drawing.Size(159, 57);
            this.btnconfirmar.TabIndex = 39;
            this.btnconfirmar.Text = "Confirmar";
            this.btnconfirmar.UseVisualStyleBackColor = true;
            this.btnconfirmar.Click += new System.EventHandler(this.button2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Livraria1.Properties.Resources.lisa;
            this.pictureBox1.Location = new System.Drawing.Point(-2, -2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(583, 312);
            this.pictureBox1.TabIndex = 40;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // txtdescricaop
            // 
            this.txtdescricaop.Location = new System.Drawing.Point(5, 101);
            this.txtdescricaop.Multiline = true;
            this.txtdescricaop.Name = "txtdescricaop";
            this.txtdescricaop.Size = new System.Drawing.Size(141, 65);
            this.txtdescricaop.TabIndex = 41;
            this.txtdescricaop.TextChanged += new System.EventHandler(this.textBox1_TextChanged_1);
            // 
            // Multa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(548, 303);
            this.Controls.Add(this.txtdescricaop);
            this.Controls.Add(this.txtnome);
            this.Controls.Add(this.lblvalor);
            this.Controls.Add(this.lblcliente);
            this.Controls.Add(this.lbldescricao);
            this.Controls.Add(this.btnconfirmar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.txtvalor);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Multa";
            this.Text = "Multa";
            this.Load += new System.EventHandler(this.Multa_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.TextBox txtvalor;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.Label lblcliente;
        private System.Windows.Forms.Label lblvalor;
        private System.Windows.Forms.Label lbldescricao;
        private System.Windows.Forms.Button btnconfirmar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtdescricaop;
    }
}