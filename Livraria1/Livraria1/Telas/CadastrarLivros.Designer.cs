﻿namespace Livraria1
{
    partial class CadastrarLivros
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCadastrar2 = new System.Windows.Forms.Button();
            this.txtedicaolivro = new System.Windows.Forms.TextBox();
            this.txtanolivro = new System.Windows.Forms.TextBox();
            this.txtautorlivro = new System.Windows.Forms.TextBox();
            this.txtnomelivro = new System.Windows.Forms.TextBox();
            this.lblNome = new System.Windows.Forms.Label();
            this.lblnomelivro = new System.Windows.Forms.Label();
            this.lblEdicao = new System.Windows.Forms.Label();
            this.lblAutor = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCadastrar2
            // 
            this.btnCadastrar2.BackColor = System.Drawing.Color.Firebrick;
            this.btnCadastrar2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrar2.Font = new System.Drawing.Font("Poor Richard", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrar2.ForeColor = System.Drawing.Color.Goldenrod;
            this.btnCadastrar2.Location = new System.Drawing.Point(185, 238);
            this.btnCadastrar2.Name = "btnCadastrar2";
            this.btnCadastrar2.Size = new System.Drawing.Size(159, 57);
            this.btnCadastrar2.TabIndex = 25;
            this.btnCadastrar2.Text = "Cadastrar Livro";
            this.btnCadastrar2.UseVisualStyleBackColor = false;
            this.btnCadastrar2.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtedicaolivro
            // 
            this.txtedicaolivro.Location = new System.Drawing.Point(139, 102);
            this.txtedicaolivro.Name = "txtedicaolivro";
            this.txtedicaolivro.Size = new System.Drawing.Size(248, 20);
            this.txtedicaolivro.TabIndex = 24;
            // 
            // txtanolivro
            // 
            this.txtanolivro.Location = new System.Drawing.Point(139, 134);
            this.txtanolivro.Name = "txtanolivro";
            this.txtanolivro.Size = new System.Drawing.Size(65, 20);
            this.txtanolivro.TabIndex = 21;
            this.txtanolivro.TextChanged += new System.EventHandler(this.txtanolivro_TextChanged);
            // 
            // txtautorlivro
            // 
            this.txtautorlivro.Location = new System.Drawing.Point(139, 67);
            this.txtautorlivro.Name = "txtautorlivro";
            this.txtautorlivro.Size = new System.Drawing.Size(248, 20);
            this.txtautorlivro.TabIndex = 20;
            this.txtautorlivro.TextChanged += new System.EventHandler(this.txtautorlivro_TextChanged);
            // 
            // txtnomelivro
            // 
            this.txtnomelivro.Location = new System.Drawing.Point(139, 36);
            this.txtnomelivro.Name = "txtnomelivro";
            this.txtnomelivro.Size = new System.Drawing.Size(248, 20);
            this.txtnomelivro.TabIndex = 19;
            this.txtnomelivro.TextChanged += new System.EventHandler(this.txtnomelivro_TextChanged);
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.lblNome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblNome.Font = new System.Drawing.Font("Poor Richard", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.Location = new System.Drawing.Point(75, 136);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(40, 18);
            this.lblNome.TabIndex = 18;
            this.lblNome.Text = "Ano:";
            // 
            // lblnomelivro
            // 
            this.lblnomelivro.AutoSize = true;
            this.lblnomelivro.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.lblnomelivro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblnomelivro.Font = new System.Drawing.Font("Poor Richard", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnomelivro.Location = new System.Drawing.Point(29, 38);
            this.lblnomelivro.Name = "lblnomelivro";
            this.lblnomelivro.Size = new System.Drawing.Size(104, 18);
            this.lblnomelivro.TabIndex = 16;
            this.lblnomelivro.Text = "Nome do livro:";
            // 
            // lblEdicao
            // 
            this.lblEdicao.AutoSize = true;
            this.lblEdicao.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.lblEdicao.Font = new System.Drawing.Font("Poor Richard", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEdicao.Location = new System.Drawing.Point(58, 102);
            this.lblEdicao.Name = "lblEdicao";
            this.lblEdicao.Size = new System.Drawing.Size(57, 18);
            this.lblEdicao.TabIndex = 15;
            this.lblEdicao.Text = "Edição:";
            // 
            // lblAutor
            // 
            this.lblAutor.AutoSize = true;
            this.lblAutor.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.lblAutor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblAutor.Font = new System.Drawing.Font("Poor Richard", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAutor.Location = new System.Drawing.Point(58, 67);
            this.lblAutor.Name = "lblAutor";
            this.lblAutor.Size = new System.Drawing.Size(51, 18);
            this.lblAutor.TabIndex = 13;
            this.lblAutor.Text = "Autor:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Livraria1.Properties.Resources._1134;
            this.pictureBox1.Location = new System.Drawing.Point(1, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(531, 316);
            this.pictureBox1.TabIndex = 26;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // CadastrarLivros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 318);
            this.Controls.Add(this.btnCadastrar2);
            this.Controls.Add(this.txtanolivro);
            this.Controls.Add(this.txtedicaolivro);
            this.Controls.Add(this.txtautorlivro);
            this.Controls.Add(this.txtnomelivro);
            this.Controls.Add(this.lblNome);
            this.Controls.Add(this.lblEdicao);
            this.Controls.Add(this.lblAutor);
            this.Controls.Add(this.lblnomelivro);
            this.Controls.Add(this.pictureBox1);
            this.Name = "CadastrarLivros";
            this.Text = "CadastrarLivro";
            this.Load += new System.EventHandler(this.Livros_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCadastrar2;
        private System.Windows.Forms.TextBox txtedicaolivro;
        private System.Windows.Forms.TextBox txtanolivro;
        private System.Windows.Forms.TextBox txtautorlivro;
        private System.Windows.Forms.TextBox txtnomelivro;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Label lblnomelivro;
        private System.Windows.Forms.Label lblEdicao;
        private System.Windows.Forms.Label lblAutor;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}