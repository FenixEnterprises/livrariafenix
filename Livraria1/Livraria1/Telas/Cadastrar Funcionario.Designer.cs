﻿namespace Livraria1
{
    partial class Cadastrar_Funcionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btncadastrar = new System.Windows.Forms.Button();
            this.txttelefone = new System.Windows.Forms.TextBox();
            this.txtendereco = new System.Windows.Forms.TextBox();
            this.txtfuncionario = new System.Windows.Forms.TextBox();
            this.lblfuncionario = new System.Windows.Forms.Label();
            this.lblendereco = new System.Windows.Forms.Label();
            this.lblcpf = new System.Windows.Forms.Label();
            this.lbltelefone = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.mskTextBoxcpf = new System.Windows.Forms.MaskedTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btncadastrar
            // 
            this.btncadastrar.BackColor = System.Drawing.Color.White;
            this.btncadastrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncadastrar.Font = new System.Drawing.Font("Poor Richard", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncadastrar.ForeColor = System.Drawing.Color.Gold;
            this.btncadastrar.Location = new System.Drawing.Point(150, 185);
            this.btncadastrar.Name = "btncadastrar";
            this.btncadastrar.Size = new System.Drawing.Size(159, 57);
            this.btncadastrar.TabIndex = 25;
            this.btncadastrar.Text = "Cadastrar";
            this.btncadastrar.UseVisualStyleBackColor = false;
            this.btncadastrar.Click += new System.EventHandler(this.button1_Click);
            // 
            // txttelefone
            // 
            this.txttelefone.Location = new System.Drawing.Point(315, 3);
            this.txttelefone.Name = "txttelefone";
            this.txttelefone.Size = new System.Drawing.Size(63, 20);
            this.txttelefone.TabIndex = 24;
            this.txttelefone.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
            // 
            // txtendereco
            // 
            this.txtendereco.Location = new System.Drawing.Point(94, 33);
            this.txtendereco.Name = "txtendereco";
            this.txtendereco.Size = new System.Drawing.Size(143, 20);
            this.txtendereco.TabIndex = 20;
            this.txtendereco.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // txtfuncionario
            // 
            this.txtfuncionario.Location = new System.Drawing.Point(94, 1);
            this.txtfuncionario.Name = "txtfuncionario";
            this.txtfuncionario.Size = new System.Drawing.Size(143, 20);
            this.txtfuncionario.TabIndex = 19;
            this.txtfuncionario.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // lblfuncionario
            // 
            this.lblfuncionario.AutoSize = true;
            this.lblfuncionario.BackColor = System.Drawing.Color.White;
            this.lblfuncionario.Font = new System.Drawing.Font("Poor Richard", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblfuncionario.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblfuncionario.Location = new System.Drawing.Point(-3, 0);
            this.lblfuncionario.Name = "lblfuncionario";
            this.lblfuncionario.Size = new System.Drawing.Size(91, 38);
            this.lblfuncionario.TabIndex = 16;
            this.lblfuncionario.Text = "Funcionário\r\n\r\n";
            this.lblfuncionario.Click += new System.EventHandler(this.label4_Click);
            // 
            // lblendereco
            // 
            this.lblendereco.AutoSize = true;
            this.lblendereco.BackColor = System.Drawing.Color.White;
            this.lblendereco.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblendereco.Font = new System.Drawing.Font("Poor Richard", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblendereco.Location = new System.Drawing.Point(-2, 35);
            this.lblendereco.Name = "lblendereco";
            this.lblendereco.Size = new System.Drawing.Size(70, 18);
            this.lblendereco.TabIndex = 15;
            this.lblendereco.Text = "Endereço";
            this.lblendereco.Click += new System.EventHandler(this.label3_Click);
            // 
            // lblcpf
            // 
            this.lblcpf.AutoSize = true;
            this.lblcpf.BackColor = System.Drawing.Color.White;
            this.lblcpf.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblcpf.Font = new System.Drawing.Font("Poor Richard", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcpf.Location = new System.Drawing.Point(243, 33);
            this.lblcpf.Name = "lblcpf";
            this.lblcpf.Size = new System.Drawing.Size(44, 18);
            this.lblcpf.TabIndex = 14;
            this.lblcpf.Text = "C.P.F.";
            this.lblcpf.Click += new System.EventHandler(this.label2_Click);
            // 
            // lbltelefone
            // 
            this.lbltelefone.AutoSize = true;
            this.lbltelefone.BackColor = System.Drawing.Color.White;
            this.lbltelefone.Font = new System.Drawing.Font("Poor Richard", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltelefone.Location = new System.Drawing.Point(243, 3);
            this.lbltelefone.Name = "lbltelefone";
            this.lbltelefone.Size = new System.Drawing.Size(66, 18);
            this.lbltelefone.TabIndex = 13;
            this.lbltelefone.Text = "Telefone";
            this.lbltelefone.Click += new System.EventHandler(this.label1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Livraria1.Properties.Resources.lisa_study_1172665637_1191264133_4525552;
            this.pictureBox1.Location = new System.Drawing.Point(1, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(399, 270);
            this.pictureBox1.TabIndex = 26;
            this.pictureBox1.TabStop = false;
            // 
            // mskTextBoxcpf
            // 
            this.mskTextBoxcpf.Location = new System.Drawing.Point(293, 35);
            this.mskTextBoxcpf.Mask = "000.000.000-00";
            this.mskTextBoxcpf.Name = "mskTextBoxcpf";
            this.mskTextBoxcpf.Size = new System.Drawing.Size(85, 20);
            this.mskTextBoxcpf.TabIndex = 27;
            this.mskTextBoxcpf.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedTextBox1_MaskInputRejected);
            // 
            // Cadastrar_Funcionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(401, 268);
            this.Controls.Add(this.mskTextBoxcpf);
            this.Controls.Add(this.btncadastrar);
            this.Controls.Add(this.lblcpf);
            this.Controls.Add(this.txttelefone);
            this.Controls.Add(this.lbltelefone);
            this.Controls.Add(this.txtendereco);
            this.Controls.Add(this.lblendereco);
            this.Controls.Add(this.lblfuncionario);
            this.Controls.Add(this.txtfuncionario);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Cadastrar_Funcionario";
            this.Text = "Cadastrar_Funcionario";
            this.Load += new System.EventHandler(this.Cadastrar_Funcionario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btncadastrar;
        private System.Windows.Forms.TextBox txttelefone;
        private System.Windows.Forms.TextBox txtendereco;
        private System.Windows.Forms.TextBox txtfuncionario;
        private System.Windows.Forms.Label lblfuncionario;
        private System.Windows.Forms.Label lblendereco;
        private System.Windows.Forms.Label lblcpf;
        private System.Windows.Forms.Label lbltelefone;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.MaskedTextBox mskTextBoxcpf;
    }
}