﻿using Livraria1.DB.Reserva;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Livraria1
{
    public partial class Reserva : Form
    {
        public Reserva()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ReservaDTO dto = new ReservaDTO();
            dto.cliente = txtnomelivro.Text;
            dto.funcionario = txtnomefuncionario.Text;

            ReservaBusiness business = new ReservaBusiness();
            business.Salvar(dto);

            MessageBox.Show("A reserva foi efetuada com sucesso");

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Reserva_Load(object sender, EventArgs e)
        {

        }
    }
}
