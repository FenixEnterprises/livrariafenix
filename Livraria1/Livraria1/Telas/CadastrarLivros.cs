﻿using Livraria1.DB.Livros;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Livraria1
{
    public partial class CadastrarLivros : Form
    {
        public CadastrarLivros()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CadLivroDTO dto = new CadLivroDTO();
            dto.nome = txtnomelivro.Text;
            dto.autor = txtautorlivro.Text;
            dto.ano = txtanolivro.Text;
            dto.edicao = txtedicaolivro.Text;
            

            CadLivroBusiness business = new CadLivroBusiness();
            business.Salvar(dto);

            MessageBox.Show("O cadastro foi efetuado com sucesso");
        }

        private void txtnomelivro_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtautorlivro_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtanolivro_TextChanged(object sender, EventArgs e)
        {

        }

        private void Livros_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
    
}
