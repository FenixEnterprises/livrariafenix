﻿namespace Livraria1
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCadastrarFuncionario = new System.Windows.Forms.Button();
            this.btnCadastarCliente = new System.Windows.Forms.Button();
            this.btnEntrarCliente = new System.Windows.Forms.Button();
            this.btnCasdastarLivro = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCadastrarFuncionario
            // 
            this.btnCadastrarFuncionario.BackColor = System.Drawing.Color.Transparent;
            this.btnCadastrarFuncionario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastrarFuncionario.Font = new System.Drawing.Font("Poor Richard", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastrarFuncionario.ForeColor = System.Drawing.Color.Gold;
            this.btnCadastrarFuncionario.Location = new System.Drawing.Point(77, 129);
            this.btnCadastrarFuncionario.Name = "btnCadastrarFuncionario";
            this.btnCadastrarFuncionario.Size = new System.Drawing.Size(136, 62);
            this.btnCadastrarFuncionario.TabIndex = 2;
            this.btnCadastrarFuncionario.Text = "Cadastrar Funcionário";
            this.btnCadastrarFuncionario.UseVisualStyleBackColor = false;
            this.btnCadastrarFuncionario.Click += new System.EventHandler(this.btnCadastrarFuncionario_Click);
            // 
            // btnCadastarCliente
            // 
            this.btnCadastarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCadastarCliente.Font = new System.Drawing.Font("Poor Richard", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCadastarCliente.ForeColor = System.Drawing.Color.Gold;
            this.btnCadastarCliente.Location = new System.Drawing.Point(77, 220);
            this.btnCadastarCliente.Name = "btnCadastarCliente";
            this.btnCadastarCliente.Size = new System.Drawing.Size(142, 62);
            this.btnCadastarCliente.TabIndex = 3;
            this.btnCadastarCliente.Text = "Cadastrar Cliente";
            this.btnCadastarCliente.UseVisualStyleBackColor = true;
            this.btnCadastarCliente.Click += new System.EventHandler(this.btnCadastarCliente_Click);
            // 
            // btnEntrarCliente
            // 
            this.btnEntrarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEntrarCliente.Font = new System.Drawing.Font("Poor Richard", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEntrarCliente.ForeColor = System.Drawing.Color.Gold;
            this.btnEntrarCliente.Location = new System.Drawing.Point(281, 220);
            this.btnEntrarCliente.Name = "btnEntrarCliente";
            this.btnEntrarCliente.Size = new System.Drawing.Size(136, 62);
            this.btnEntrarCliente.TabIndex = 4;
            this.btnEntrarCliente.Text = "Entrar Cliente";
            this.btnEntrarCliente.UseVisualStyleBackColor = true;
            this.btnEntrarCliente.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnCasdastarLivro
            // 
            this.btnCasdastarLivro.BackColor = System.Drawing.Color.Transparent;
            this.btnCasdastarLivro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCasdastarLivro.Font = new System.Drawing.Font("Poor Richard", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCasdastarLivro.ForeColor = System.Drawing.Color.Gold;
            this.btnCasdastarLivro.Location = new System.Drawing.Point(281, 129);
            this.btnCasdastarLivro.Name = "btnCasdastarLivro";
            this.btnCasdastarLivro.Size = new System.Drawing.Size(142, 58);
            this.btnCasdastarLivro.TabIndex = 5;
            this.btnCasdastarLivro.Text = "Cadastrar Livro";
            this.btnCasdastarLivro.UseVisualStyleBackColor = false;
            this.btnCasdastarLivro.Click += new System.EventHandler(this.btnCasdastarLivro_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Livraria1.Properties.Resources.efa6bfa3b3e6088f2a4937808a198916;
            this.pictureBox2.Location = new System.Drawing.Point(55, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(416, 92);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Livraria1.Properties.Resources._98cf183a03dbb58bf43bb23b4acf1af8;
            this.pictureBox1.Location = new System.Drawing.Point(-2, -2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(615, 433);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(499, 426);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnCasdastarLivro);
            this.Controls.Add(this.btnEntrarCliente);
            this.Controls.Add(this.btnCadastarCliente);
            this.Controls.Add(this.btnCadastrarFuncionario);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Menu";
            this.Text = "Menu";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnCadastrarFuncionario;
        private System.Windows.Forms.Button btnCadastarCliente;
        private System.Windows.Forms.Button btnEntrarCliente;
        private System.Windows.Forms.Button btnCasdastarLivro;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}